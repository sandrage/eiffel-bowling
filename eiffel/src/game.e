note
    description: "Esempio base per iniziare"
    author: "Mattia Monga"

class
    GAME

create
    make

feature -- sezione: puo` contenere molte feature
	score_value:INTEGER
	rolls:ARRAY[INTEGER]
	current_roll:INTEGER
	make
		do
			score_value:=0
			create rolls.make(0,19)
			current_roll:=0
		end

	roll(pins:INTEGER)
		do
			rolls[current_roll]:=pins
			current_roll:=current_roll+1
		end
	is_spare(frame_index:INTEGER):BOOLEAN
		do
			Result:=(rolls[frame_index]+rolls[frame_index+1]=10)
		end
	spare_bonus(frame_index:INTEGER):INTEGER
		do
			Result:=rolls[frame_index+2]
		end
	strike_bonus(frame_index:INTEGER):INTEGER
		do
			Result:=rolls[frame_index+1]+rolls[frame_index+2]
		end
	default_bonus(frame_index:INTEGER):INTEGER
		do
			Result:=rolls[frame_index]+rolls[frame_index+1]
		end
	is_strike(frame_index:INTEGER):BOOLEAN
		do
			Result:=rolls[frame_index]=10
		end
	score:INTEGER
		local
			i:INTEGER
			frame:INTEGER
		do
			score_value:=0
			from
				frame:=0
				i:=0
			until
				frame>=10
			loop
				if is_spare(i)
				then
					score_value:=score_value+10+spare_bonus(i)
					i:=i+2
				else
					if is_strike(i) then
						score_value:=score_value+10+strike_bonus(i)
						i:=i+1
					else
						score_value:=score_value+default_bonus(i)
						i:=i+2
					end
				end

				frame:=frame+1
			end
			Result:=score_value
		end

end
