note
	description: "[
		Eiffel tests that can be executed by testing tool.
	]"
	author: "EiffelStudio test wizard"
	date: "$Date$"
	revision: "$Revision$"
	testing: "type/manual"

class
	GAME_TEST_SET

inherit
	EQA_TEST_SET

feature -- Test routines

	test_gutter_game
		note
			testing:  "covers/{GAME}"
		local
			game:GAME
		do
			create game.make
			rolls_many(20,0,game)
			assert("Check that score is 0 when no pins were rolled",game.score=0)
		end
	test_all_ones
		note
			testing: "covers/{GAME}"
		local
			game:GAME
		do
			create game.make
			rolls_many(20,1,game)
			assert("Check that the score increses with pins rolled",game.score=20)
		end
	test_on_spare
		note
			testing: "/covers/{GAME}"
		local
			game:GAME
		do
			create game.make
			roll_spare(game)
			game.roll (3)
			rolls_many(17,0,game)
			assert("Check on spare",game.score=16)

		end
	test_on_strike
		local
			game:GAME
		do
			create game.make
			game.roll (10)
			game.roll (3)
			game.roll (4)
			rolls_many(16,0,game)
			assert("Check on strike score",game.score=24)
		end
	roll_spare(game:GAME)
		do
			game.roll (5)
			game.roll (5)
		end
	rolls_many(n:INTEGER;pins:INTEGER;game:GAME)
		local
			i:INTEGER
		do
			from
				i:=0
			until
				i>=n
			loop
				game.roll (pins)
				i:=i+1
			end
		end
	test_perfect_game
		local
			game: GAME
		do
			create game.make
			rolls_many(12,10,game)
			assert("Perfect game",game.score=300)
		end

end


